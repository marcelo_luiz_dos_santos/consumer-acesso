package br.com.itau.ConsumerAcesso.consumer;

import br.com.itau.AcessoCaseAcesso.Acesso.DTO.AcessoKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-marcelo-luiz-1", groupId = "MarceloLuiz")
    public void receber(@Payload AcessoKafka acesso) throws IOException {
        String mensagem = "";
        if (acesso.getAcessoLiberado().equals(true)){
            mensagem = "Cliente " + acesso.getClienteId() + " LIBERADO para acesso na porta " + acesso.getPortaId();
            System.out.println(mensagem);
        }else{
            mensagem = "Cliente " + acesso.getClienteId() + " NEGADO para acesso na porta " + acesso.getPortaId();
            System.out.println(mensagem);
        }
        ArquivoLog arquivoLog = new ArquivoLog();
        arquivoLog.GravaArquivoLog(mensagem);
    }
}
